//use serde_json::Value;
//use reqwest::blocking::Client;
use reqwest::header::ACCEPT;
//use std::collections::HashMap;

fn main() -> Result<(), Box<dyn std::error::Error>> {

    let mut key = std::fs::read_to_string("./api_key.txt").unwrap();
    key.pop();

    //let params = [("access_token", &key)];

    /*
    let mut body = HashMap::new();
        body.insert("Context", "string");
        body.insert("Mode", "string");
        body.insert("Text", "# String\n > quoted");
        body.insert("Wiki", "true");
    */
    let client = reqwest::blocking::Client::new();
    let mut post_string: String = "https://tea.jdogg.club/api/v1/repos/jared/rust-projects/mirror-sync?access_token=".to_string();
    post_string.push_str(&key);
    client.post(&post_string)
        .header(ACCEPT, "text/html")
        .send()?;

    Ok(())
}
