use std::{env, io};

use actix_files::Files;
use actix_web::{web::Data, get, App, HttpServer, HttpResponse, Responder};
use handlebars::Handlebars;

#[macro_use]
extern crate serde_json;


#[get("/")]
async fn index(hb: Data<Handlebars<'_>>) -> impl Responder {
    let data = json!({
        "title": "Test Title!"
    });

    let html = hb.render("index", &data).unwrap();

    HttpResponse::Ok()
        .content_type("text/html")
        .body(html)
}

#[actix_web::main]
async fn main() -> io::Result<()> {
    let addr = "127.0.0.1:2000";

    let template_service = {
        let mut handlebars = Handlebars::new();

        handlebars
            .register_templates_directory(".html", "web/templates")
            .unwrap();

        Data::new(handlebars)
    };

    println!("building at http://{0}", addr);

    let server = move || App::new()
        .app_data(template_service.clone())
        .service(Files::new("/public", "web/public").show_files_listing())
        .service(index);

    HttpServer::new(server)
        .bind(addr)?
        .run()
        .await
}