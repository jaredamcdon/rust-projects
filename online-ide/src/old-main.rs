//back end
use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};

//templating
use handlebars::Handlebars;

//json macro/formatting
#[macro_use]
extern crate serde_json;

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok().body(req_body)
}

async fn manual_hello( hb: web::Data<Handlebars<'_>> ) -> impl Responder {
    let data = json!({
       "title": "Test Title!" 
       });

    let index = hb.render("data", &data).unwrap();

    HttpResponse::Ok()
        .content_type("text/html")    
        .body(index)

}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let addr = "127.0.0.1:2000";
    // handlebars templating configuration
    let template_service = {
        let mut handlebars = Handlebars::new();
        handlebars
            .register_template_directory(".html", "web/templates")
            .unwrap();
        //let handlebars_ref = web::Data::new(handlebars);
        Data::new(handlebars)
    }

    println!("building at http://{0}", addr);

    /*HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(echo)
            .app_data(handlebars_ref.clone())
            .route("/hey", web::get().to(manual_hello))
    })*/

    let server = move || App::new()
        .app_data(template_service.clone())
        .service(Files::new("/public", "web/public").show_files_listing())
        .service(manual_hello);
    
    .bind(addr)?
        .run()
        .await
}
