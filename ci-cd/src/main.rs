use reqwest::header::ACCEPT;
use std::env;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    
    let args: Vec<String> = env::args().collect();
    let key = &args[1];
    let name = &args[2];

    let client = reqwest::blocking::Client::new();
    let mut post_string: String = "https://tea.jdogg.club/api/v1/repos/jared/".to_string();
    post_string.push_str(&name);
    post_string.push_str("/mirror-sync?access_token=");
    post_string.push_str(&key);
    client.post(&post_string)
        .header(ACCEPT, "text/html")
        .send()?;

    Ok(())
}
