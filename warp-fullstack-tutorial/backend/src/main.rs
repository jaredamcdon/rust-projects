use mobc::{Connection, Pool};
use mobc_postgres::{tokio_postgres, PgConnectionManager};
use std::convert::Infallible;
use tokio_postgres::NoTls;
use warp::{
    http::{header, Method},
    Filter, Rejection,
};

mod db;
mod error;
mod handler;

type Result<T> = std::result::Result<T, Rejection>;
type DBCon = Connection<PgConnectionManager<NoTls>>;
type DBPool = Pool<PgConnectionManager<NoTls>>;

#[tokio::main]
async fn main() {
    let db_pool = db::create_pool() // calls db/mod.rs.create_pool
        .expect("database pool can be created");
    
        db::init_db(&db_pool)
            .await
            .expect("database can be initialized");

    /* route initialization */
    let pet = warp::path!("owner" / i32 / "pet");  // GET /owner/$owner_id/pet -> lists all pet of given owner || POST /owner/$owner_id/pet creates pet for owner || /owner/$owner_id  -> returns owner with given ID
    let pet_param = warp::path!("owner" / i32 / "pet" / i32); // DELETE /owner/$owner_id/pet/$pet_id -> deletes pet by id and owner id 
    let owner = warp::path("owner"); // GET /owner -> lists all owners || POST /owner creates owner

    let pet_routes = pet
        .and(warp::get()) // at GET pet route initialization
        .and(with_db(db_pool.clone()))
        .and_then(handler::list_pets_handler) //list pets
        .or(pet
            .and(warp::post()) // at POST pet route initialization
            .and(warp::body::json())
            .and(with_db(db_pool.clone()))
            .and_then(handler::create_pet_handler) // calls handler.rs.create_pet_handler
        )
        .or(pet_param
            .and(warp::delete()) // at DELETE
            .and(with_db(db_pool.clone()))
            .and_then(handler::delete_pet_handler) // calls handler.rs.delete_pet_handler
        );

    let owner_routes = owner
        .and(warp::get()) // at GET
        .and(warp::path::param())
        .and(with_db(db_pool.clone()))
        .and_then(handler::fetch_owner_handler) // calls handler.rs.fetch_owner_handler
        .or(owner
            .and(warp::get()) // at GET
            .and(with_db(db_pool.clone()))
            .and_then(handler::list_owners_handler) // call handler.rs.list_owners_handler
        )
        .or(owner
            .and(warp::post()) // at POST
            .and(warp::body::json())
            .and(with_db(db_pool.clone()))
            .and_then(handler::create_owner_handler) // call handler.rs.create_owner_handler
        );

    // set up cors config and server on localhost:8000
    let routes = pet_routes // ties back to pet_routes
        .or(owner_routes) // ties back to owner_routes
        .recover(error::handle_rejection)
        .with(
            warp::cors()
                .allow_credentials(true)
                .allow_methods(&[
                    Method::OPTIONS,
                    Method::GET,
                    Method::POST,
                    Method::DELETE,
                    Method::PUT,
                ])
                .allow_headers(vec![header::CONTENT_TYPE, header::ACCEPT])
                .expose_headers(vec![header::LINK])
                .max_age(300)
                .allow_any_origin(),
        );
    warp::serve(routes).run(([127, 0, 0, 1], 8000)).await;
}

// allows warp to pass data to handler
fn with_db(db_pool: DBPool) -> impl Filter<Extract = (DBPool,), Error = Infallible> + Clone {
    warp::any()
        .map(move || db_pool.clone()) // helps pass warp connection pool
}