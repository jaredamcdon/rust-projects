mod print;
mod vars;
mod types;
mod strings;

//rush crash course | Rustlang 38:11

fn main() {
    print::run();
    vars::run();
    types::run();
    strings::run();
}
