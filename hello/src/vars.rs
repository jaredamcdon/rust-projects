//Variables hold primitive data or references to data
//Variables are immutable by default (cant reassign)
//rust is a block-scoped language

pub fn run() {

    let name = "Jared";
    //mut allows variable to be changed
    let mut age = 20;

    println!("My name is {}, I am {}", name, age);

    age = 21;

    println!("My name is {}, I am {}", name, age);

    //const requires variable rtpe
    const ID: i32 = 001;

    println!("ID: {}", ID);

    //assign multiple vars
    let (my_name, my_age) = ("jdogg", 20);

    println!("Name: {}, Age: {}", my_name, my_age);
}
