//primitive string = immutable fixed-length string somewhere in memory
//string = growable, heap allocated data structure - Use when you need to modify or own string data

pub fn run(){

    let hello_prim  = "Hello ";

    let mut hello_str = String::from("Hello ");

    //get length
    println!("Length: {}", hello_str.len());

    //push char
    hello_str.push('W');

    //push string
    hello_str.push_str("orld!");

    println!("{}", hello_str);

}
