pub fn  run() {
    // print to console
    println!("Hello from the print.rs file");
    //number formatting
    println!("Number: {}", 1);
    //basic formatting
    println!("{} is from {}", "Jared", "Indianapolis");
    
    //positional parameters
    println!(
        "{0} is from {1} and {0} likes to {2}",
        "Jared", "Indianpolis", "code"
        );
    //Named args
    println!(
        "{name} likes to play {activity}",
        name = "Jared",
        activity = "Basketball"
        );
    //placeholder traits
    println!("Binary: {:b}, Hex: {:x}, Octal: {:o}", 10, 10, 10);

    //placeholder for debug trait
    println!("{:?}", (12, true, "hello"));
    
    //basic math
    println!(
        "{} + {} = {}",
        10, 10, 10 + 10
        );
}

