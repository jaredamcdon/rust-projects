#![feature(proc_macro_hygiene, decl_macro)]

extern crate chrono;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

pub mod cors;
pub mod models;
pub mod routes;
pub mod schema; // ignore errors for now

// this registers db with rocket, returns a "Fairing" that can be '.attach'd to
// rocket application to set up a connection pool for it and automatically manage it for you

#[database("rocket_app")]
pub struct DbConn(diesel::MysqlConnection);

fn main() {
    rocket::ignite()
        .mount("/", routes![
            routes::index,
            routes::create_page_view,
            routes::list_page_views,
        ])
        .attach(DbConn::fairing())
        .attach(cors::CorsFairing)
        .launch();
}
