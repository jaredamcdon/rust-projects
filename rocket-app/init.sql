CREATE USER 'rocket'@"%" IDENTIFIED BY 'rocket';
CREATE DATABASE rocket_app;
GRANT ALL PRIVILEGES ON rocket_app.* to rocket;