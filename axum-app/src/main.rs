use axum::{
    extract::Query,
    http::StatusCode,
    response::{Html, IntoResponse, Response},
    Json,
    routing::get,
    Router
};
use std::net::SocketAddr;
use serde::Deserialize;
use rand::{thread_rng, Rng};
use serde_json::{json, Value};
use askama::Template;

#[tokio::main]
async fn main() {
    
    //let mut handlebars = Handlebars::new();
    //handlebars
    //    .register_templates_directory(".html", "web/templates")
    //    .unwrap();

    let app = Router::new()
        .route("/", get(index))
        .route("/rand", get(rand))
        .route("/api", get(api))
        .route("/askama", get(templating));

    let addr = SocketAddr::from(([127, 0, 0, 1], 3000));
    println!("Listening on {}", addr);

    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await.
        unwrap();

}

#[derive(Template)]
#[template(path="template.html")]
struct FirstAskamaTemplate {
    title: &'static str
}

#[derive(Deserialize)]
struct RangeParameters {
    start: usize,
    end: usize
}

async fn index() -> Html<&'static str> {
    Html(include_str!("../index.html"))
}

async fn rand(Query(range): Query<RangeParameters>) -> Html<String> {
    let random_number = thread_rng().gen_range(range.start..=range.end);

    // http://127.0.0.1:3000/?start=50&end=100
    Html(format!("<h1>Random Number: {}</h1>", random_number))
}

async fn api() -> Json<Value> {
    Json(json!({ "data": 42 }))
}

async fn templating() -> impl IntoResponse {
    println!("running templating");
    let template = FirstAskamaTemplate{ title: "templated title" };
    HtmlTemplate(template)
}

struct HtmlTemplate<T>(T);

impl<T> IntoResponse for HtmlTemplate<T>
where
    T: Template,
{
    fn into_response(self) -> Response {
        match self.0.render() {
            Ok(html) => Html(html).into_response(),
            Err(err) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Failed to render template. Error: {}", err),
            )
                .into_response(),
        }
    }
}
