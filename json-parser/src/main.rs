use std::fs;
use std::env;
use serde::{Deserialize, Serialize};
//use serde_json::Result;

#[derive(Serialize, Deserialize)]
struct ContactFormData {
    phone: u64,
    name: String,
    email: String,
    message: String
}


fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];

    println!("in file {}", file_path);

    let file_content = fs::read_to_string(file_path)
        .expect("Should be able to read the file");

    println!("With text:\n{file_content}");

    let json: ContactFormData=
        serde_json::from_str(&file_content)
            .expect("JSON should be serializable");

    //println!("{}", json);

    println!("{}: {}", "phone", json.phone);
}
