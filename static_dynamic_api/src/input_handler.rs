use std::io::Read;
use serde_json::{Result, Value};

pub fn get_json() {
    
     fn convert(data:&str) -> Result<serde_json::value::Value> {
        let data_obj: Value = serde_json::from_str(data)?;
        //let data_obj: serde_json::Value = serde_json::from_str(&data)
     
        println!("{}", data_obj);
        
        let to_return = Ok(data_obj);
        return to_return;
    
    }

    
    let mut file = std::fs::File::open("./inputs/config.json").unwrap();
    let mut contents = String::new();
    file.read_to_string(&mut contents).unwrap();
    print!("{}", contents);
    
    // ampersand symbolizes borrowing a variable
    let config = convert(&contents);

    //println!("Name: {}", config["name"]);
    println!("{}", config["Value"]);
}
