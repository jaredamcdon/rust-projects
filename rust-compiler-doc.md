
### just a quick writeup of basic commands **

---

### recommended gitignore
.target/*

### note
unsure difference between init and new

#### init new project hello_world
cargo init --bin hello_world

#### start a new project hello_world
$ cargo new hello_world

#### compile and run a program
$ cargo run

#### compile a program
$ cargo build

#### run the compiled program
$ ./target/debug/hello_world

#### compile an official release
$ cargo build --release

#### official release location
$ ./target/release/hello_world

#### add a dependency
\/ cargo.toml file \/
[dependencies]
time = "0.1.12"
$ cargo run


### diesel ORM

#### install diesel CLI
$ cargo install diesel_cli --no-default-features --features mysql

#### set db env var (sql)
export DATABASE_URL="mysql://user:pass@host/database"

#### init migration
diesel migration generate initialize
 - creates:
   - up.sql
   - down.sql

#### run migration
diesel migration run
 - creates src/schema.rs

#### redo migration (rollback)
diesel migration redo
