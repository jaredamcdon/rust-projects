use warp::{self, Filter};
use console::Style;

mod routes;
mod handlers;
mod api;

use self::{
    routes::{
        hello_route,
    },
    handlers::{
        hello_handler
    },
};

#[cfg(test)] mod tests;

#[tokio::main]
async fn main() {
    let target: String = "127.0.0.1:8000"
        .parse()
        .unwrap();

    let blue = Style::new()
        .blue();

    let end = hello!()
        .with(warp::log("hello"));

    println!("\nRust Warp Server live at {}", blue.apply_to(&target));
    println!("endpoint: 127.0.0.1:8000/hello/name");

    warp::serve(end).run(([0,0,0,0], 8000)).await;
}
